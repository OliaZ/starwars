import { Component, OnInit } from '@angular/core';
import { Film } from '../common/films-service/film.service';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {
  film: Film;
  filmId: number;

  constructor() {
  }

  ngOnInit() {
  }

}
