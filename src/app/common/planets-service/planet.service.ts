import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggerService } from '../logger.service';

export class Planet {
  constructor() {}
}
@Injectable()
export class PlanetService {
  
  constructor(private httpClient: HttpClient,
    private log: LoggerService) { }

  getPlanet(id : number): Observable<Planet> {
    this.log.log('PlanetService', `returning a planet`)
    return this.httpClient.get<Planet>('https://swapi.co/api/planets/' + id)         
  }
}
