import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggerService } from '../logger.service';

export class People {
  constructor() {}
}
@Injectable()
export class PeopleService {

  constructor(private httpClient: HttpClient,
    private log: LoggerService) { }

    getPeople(id : number): Observable<People> {
      this.log.log('PeopleService', `returning a person`)
      return this.httpClient.get<People>('https://swapi.co/api/people/' + id)         
    }
}    


