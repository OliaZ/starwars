import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggerService } from '../logger.service';


export class Starship{
  constructor() {}
}

@Injectable()
export class StarshipsService {

  constructor(private httpClient: HttpClient,
    private log: LoggerService) { }

  getStarship(id : number): Observable<Starship> {
    this.log.log('StarshipService', `returning a starship`)
    return this.httpClient.get<Starship>('https://swapi.co/api/starships/' + id)         
  }
}
