import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggerService } from '../logger.service';


export class Film{
  constructor() {}
}

@Injectable()
export class FilmService {

  constructor(private httpClient: HttpClient,
    private log: LoggerService) { }

  getFilm(id : number): Observable<Film> {
    this.log.log('FilmService', `returning a film`)
    return this.httpClient.get<Film>('https://swapi.co/api/films/' + id)         
  }
}
