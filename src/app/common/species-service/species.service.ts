import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggerService } from '../logger.service';


export class Species{
  constructor() {}
}

@Injectable()
export class SpeciesService {
  constructor(private httpClient: HttpClient,
    private log: LoggerService) { }

  getSpecies(id : number): Observable<Species> {
    this.log.log('SpeciesService', `returning a species`)
    return this.httpClient.get<Species>('https://swapi.co/api/species/' + id)         
  }
}
