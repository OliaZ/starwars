import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggerService } from '../logger.service';


export class Vehicle {
  constructor() {}
}
@Injectable()
export class VehiclesService {

  constructor(private httpClient: HttpClient,
    private log: LoggerService) { }

  getVehicle(id : number): Observable<Vehicle> {
    this.log.log('VehicleService', `returning a vehicle`)
    return this.httpClient.get<Vehicle>('https://swapi.co/api/vehicles/' + id)         
  }
}
