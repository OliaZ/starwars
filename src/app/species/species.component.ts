import { Component, OnInit } from '@angular/core';
import { Species } from '../common/species-service/species.service';

@Component({
  selector: 'app-species',
  templateUrl: './species.component.html',
  styleUrls: ['./species.component.css']
})
export class SpeciesComponent implements OnInit {
  species: Species;
  speciesId: number;

  constructor() { }

  ngOnInit() {
  }

}
