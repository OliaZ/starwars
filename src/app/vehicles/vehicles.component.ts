import { Component, OnInit } from '@angular/core';
import { Vehicle } from '../common/vehicles-service/vehicles.service';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent implements OnInit {

  vehicle: Vehicle;
  vehicleId: number;

  constructor() { }

  ngOnInit() {
  }

}
