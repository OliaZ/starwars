import { Component, OnInit } from '@angular/core';
import { Planet } from '../common/planets-service/planet.service';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.css']
})
export class PlanetsComponent implements OnInit {

  planet: Planet;
  planetId: number;

  constructor() { }

  ngOnInit() {
  }

}
