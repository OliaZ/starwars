import { Component } from '@angular/core';
import { PeopleService, People } from './common/people-service/people.service';
import { FilmService, Film } from './common/films-service/film.service';
import { PlanetService, Planet } from './common/planets-service/planet.service';
import { SpeciesService, Species } from './common/species-service/species.service';
import { StarshipsService, Starship } from './common/starships-service/starships.service';
import { VehiclesService, Vehicle } from './common/vehicles-service/vehicles.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'StarWars';

  //people
  people: People;
  peopleId: number;
  constructor(private ps: PeopleService, 
              private fs: FilmService, 
              private pls: PlanetService,
              private ss: SpeciesService,
              private sts: StarshipsService,
              private vs: VehiclesService){}


  getPeople() {
    this.ps.getPeople(this.peopleId).subscribe(response => {
      console.log(response);
      this.people = response;
    })
  }

  //films
  film: Film;
  filmId: number;
  getFilm() {
    this.fs.getFilm(this.filmId).subscribe(response => {
      console.log(response);
      this.film = response;
    })
  }

  //planets
  planet: Planet;
  planetId: number;
  getPlanet() {
    this.pls.getPlanet(this.planetId).subscribe(response => {
      console.log(response);
      this.planet = response;
    })
  }

    //species
    species: Species;
    speciesId: number;
    getSpecies() {
      this.ss.getSpecies(this.speciesId).subscribe(response => {
        console.log(response);
        this.species = response;
      })
    }

    //Starships
    starship: Starship;
    starshipId: number;
    getStarship() {
      this.sts.getStarship(this.starshipId).subscribe(response => {
        console.log(response);
        this.starship = response;
      })
    }

    //Vehicles
    vehicle: Vehicle;
    vehicleId: number;
    getVehicle() {
      this.vs.getVehicle(this.vehicleId).subscribe(response => {
        console.log(response);
        this.vehicle = response;
      })
    } 
    
}
