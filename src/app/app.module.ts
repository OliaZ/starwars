import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';

import { PeopleComponent } from './people/people.component';

import { PeopleService } from './common/people-service/people.service';
import { PlanetService } from './common/planets-service/planet.service';
import { FilmService } from './common/films-service/film.service';
import { SpeciesService } from './common/species-service/species.service';
import { StarshipsService } from './common/starships-service/starships.service';
import { VehiclesService } from './common/vehicles-service/vehicles.service';

import { AppRoutingModule } from './app-routing.module';
import { FilmsComponent } from './films/films.component';
import { PlanetsComponent } from './planets/planets.component';
import { SpeciesComponent } from './species/species.component';
import { StarshipsComponent } from './starships/starships.component';
import { VehiclesComponent } from './vehicles/vehicles.component';

@NgModule({
  declarations: [
    AppComponent,
    PeopleComponent,
    FilmsComponent,
    PlanetsComponent,
    SpeciesComponent,
    StarshipsComponent,
    VehiclesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [PeopleService,
              FilmService,
              PlanetService,
              SpeciesService,
              StarshipsService,
              VehiclesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
