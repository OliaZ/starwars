import { Component, OnInit } from '@angular/core';
import { Starship } from '../common/starships-service/starships.service';

@Component({
  selector: 'app-starships',
  templateUrl: './starships.component.html',
  styleUrls: ['./starships.component.css']
})
export class StarshipsComponent implements OnInit {

  starship: Starship;
  starshipId: number;
  
  constructor() { }

  ngOnInit() {
  }

}
