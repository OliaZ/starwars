import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PeopleComponent } from './people/people.component';
import {FilmsComponent } from './films/films.component';
import {PlanetsComponent } from './planets/planets.component';
import {SpeciesComponent } from './species/species.component';
import {StarshipsComponent } from './starships/starships.component';
import {VehiclesComponent } from './vehicles/vehicles.component';

const routes: Routes = [
  //{path: '', component: HomeComponent},             // default rout
  {path: 'film', component: FilmsComponent},
  {path: 'people', component: PeopleComponent },
  {path: 'planet', component: PlanetsComponent },
  {path: 'species', component: SpeciesComponent },  
  {path: 'starship', component: StarshipsComponent},
  {path: 'vehicle', component: VehiclesComponent}
  //{path: '**', redirectTo: '/home' }               // wildcard error route
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
